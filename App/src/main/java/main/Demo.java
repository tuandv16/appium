/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author TuanDV
 */
public class Demo {
    AndroidDriver driver;
    String fileName = "appTest.apk";
    File MyViettel = new File("src/main/resources/" + fileName);

    @BeforeTest
    public void beforeTest() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("app", MyViettel);
        // khai báo platform
        cap.setCapability("platformName", "ANDROID");
        // khai báo deviceName
        cap.setCapability("deviceName", "Real Device");
        // Tìm app và cài đặt app
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test(priority = 1) // priority để ưu tiên case nào chạy trước
    // case này mình test trường hợp nhập sai tài khoản
    public void SignInWithIncorrectAccount() {
        System.out.println(driver.currentActivity());

        // Lấy Activity của màn hình hiện tại
        String currentScreen = driver.currentActivity();

        // Tìm và click button Skype Name có resource-id là com.skype.raider:id/sign_in_skype_btn
        driver.findElementById("com.skype.raider:id/sign_in_skype_btn").click();

        // Lấy Activity của màn hình sau khi click button Skype Name
        String screenSignIn = driver.currentActivity();

        // So sánh để kiểm tra có chuyển màn hình hay không
        assertNotEquals(currentScreen, screenSignIn);

        // Nhập user name
        driver.findElementById("com.skype.raider:id/signin_skypename").sendKeys("thuy.thuy123");

        // Nhập mật khẩu
        driver.findElementById("com.skype.raider:id/signin_password").sendKeys("@12345678");

        // Nhấn nút sign in
        driver.findElementById("com.skype.raider:id/sign_in_btn").click();

        // Lấy câu thông báo lỗi
        String message = driver.findElementById("com.skype.raider:id/error_message").getText();

        // Kiểm tra câu thông báo có đúng hay không
        assertEquals(message, "Oops, please check your details.");
    }

    @Test(priority = 2)
    // case này mình test trường hợp đăng nhập với account đúng
    public void SignInWithcorrectAccount() throws InterruptedException {
        String currentScreen = driver.currentActivity();
        
        // Xoá text hiện tại trên textbox username // vì cho chạy tiếp case trên nên cần xoá
        driver.findElementById("com.skype.raider:id/signin_skypename").clear();
        
        // Nhập lại username
        driver.findElementById("com.skype.raider:id/signin_skypename").sendKeys("thuy.thuy489");
        
        // Nhấn nút Sign in
        driver.findElementById("com.skype.raider:id/sign_in_btn").click();
        
        // Đợi tìm thấy button Add friend
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.skype.raider:id/mnv_add_friends_button")));
        
        // Nhấn nut Back của máy
        // vì hiện tại đăng nhập lần đầu tiền màn hình tiếp theo có activity giống màn hình trước
        driver.navigate().back();
        
        // so sánh việc chuyển màn hình
        assertNotEquals(currentScreen, driver.currentActivity());
    }
    
    @AfterTest
    public void afterTest() {
        driver.quit();
    }
}
